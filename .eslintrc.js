module.exports = {
    "env": {
        "browser": true,
        "commonjs": true,
        "jest": true
    },
    "parser": "babel-eslint",
    "extends": "eslint:recommended",
    "parserOptions": {
        "ecmaVersion": 2015,
        "sourceType": "module",
        "ecmaFeatures": {
            "jsx": true,
            "allowImportExportEverywhere": true
        },
    
        
    },
    "plugins": [
        "react"
    ],
    "rules": {
        "no-unused-vars": "off"
        
        
    }
};
